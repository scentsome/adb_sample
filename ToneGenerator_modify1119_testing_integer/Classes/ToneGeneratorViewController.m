//
//  ToneGeneratorViewController.m
//  ToneGenerator
//
//  Created by Matt Gallagher on 2010/10/20.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import "ToneGeneratorViewController.h"
#import <AudioToolbox/AudioToolbox.h>

BOOL changeFreq=NO;
int count = 0 ;
double theta_increment, previousIncrement;
static int innerCount = 0;
BOOL isSendingData= NO;
BOOL isDataReady = NO;

void changeFrequency(ToneGeneratorViewController *viewController)
{
    switch (count % 4) {
        case 0:
            theta_increment =1*M_PI * viewController->frequency / viewController->sampleRate;
            break;
        case 1:
            theta_increment =2*M_PI * viewController->frequency / viewController->sampleRate;
            break;
        case 2:
            theta_increment =4*M_PI * viewController->frequency / viewController->sampleRate;
            break;
        case 3:
            theta_increment =8*M_PI * viewController->frequency / viewController->sampleRate;
            break;
        default:
            break;
    }
}

void sendData(ToneGeneratorViewController *viewController, int signal)
{
    switch (signal) {
        case 0:
            theta_increment =1*M_PI * viewController->frequency / viewController->sampleRate;
            break;
        case 1:
            theta_increment =4*M_PI * viewController->frequency / viewController->sampleRate;
            break;
        case 8:
            theta_increment =8*M_PI * viewController->frequency / viewController->sampleRate;
            break;
        default:
            theta_increment =2*M_PI * viewController->frequency / viewController->sampleRate;
            break;
    }
}

OSStatus RenderTone(
	void *inRefCon, 
	AudioUnitRenderActionFlags 	*ioActionFlags, 
	const AudioTimeStamp 		*inTimeStamp, 
	UInt32 						inBusNumber, 
	UInt32 						inNumberFrames, 
	AudioBufferList 			*ioData)

{
    innerCount++;
	// Fixed amplitude is good enough for our purposes
	const double amplitude = 0.25;
    
	ToneGeneratorViewController *viewController =
		(ToneGeneratorViewController *)inRefCon;
	double theta = viewController.theta;
   


//    if (isDataReady) {
//        if ([viewController.toSendData count]>0) {
//            int signal =[viewController.toSendData[0] integerValue];
//            sendData(viewController, signal);
//        }else{
//            isSendingData = NO;
//            isDataReady = NO;
//        }
//    }
//   else{
//        theta_increment =8*M_PI * viewController->frequency / viewController->sampleRate;
//    }
//    
//    
//
    if (!isDataReady) {
        theta_increment =8*M_PI * viewController->frequency / viewController->sampleRate;
    }else{
        
    }
	const int channel = 0;
	Float32 *buffer = (Float32 *)ioData->mBuffers[channel].mData;
//
//    buffer[0] = sin(theta) * amplitude;
//    
//    theta += theta_increment;

    
	for (UInt32 frame =0; frame < inNumberFrames; frame++)
	{

        
        buffer[frame] = sin(theta) * amplitude;
        
        theta += theta_increment;
            if (theta > 2.0 * M_PI) {
//                theta -= 2.0 * M_PI;
                theta = 0;
                if (isDataReady) {
                    if([viewController.toSendData count]>0){
                        [viewController.toSendData removeObjectAtIndex:0];
                        if ( [viewController.toSendData count]>0) {
                            int signal =[viewController.toSendData[0] integerValue];
                            //                        NSLog(@"%@",viewController.toSendData);
                            sendData(viewController,signal);
                            
                        }else{
                            theta_increment =8*M_PI * viewController->frequency / viewController->sampleRate;
                            isDataReady = NO;
                            isSendingData = NO;
                            //                        viewController.toSendData = [@[] mutableCopy];
                        }
                        
                    }else{
                        isDataReady = NO;
                        isSendingData = NO;
                    }
                }
                else{
                    theta_increment =8*M_PI * viewController->frequency / viewController->sampleRate;
                   
//                    viewController.toSendData = [@[] mutableCopy];
                }
                
                
            }
        

        viewController.theta = theta;

        
	}
	// Store the theta back in the view controller

	return noErr;
}

void ToneInterruptionListener(void *inClientData, UInt32 inInterruptionState)
{
	ToneGeneratorViewController *viewController =
		(ToneGeneratorViewController *)inClientData;
	
	[viewController stop];
}

@implementation ToneGeneratorViewController{
    
}

@synthesize frequencySlider;
@synthesize playButton;
@synthesize frequencyLabel;
@synthesize theta;

- (IBAction)sliderChanged:(UISlider *)slider
{
	frequency = slider.value;
	frequencyLabel.text = [NSString stringWithFormat:@"%4.1f Hz", frequency];
}

- (void)createToneUnit
{
	// Configure the search parameters to find the default playback output unit
	// (called the kAudioUnitSubType_RemoteIO on iOS but
	// kAudioUnitSubType_DefaultOutput on Mac OS X)
	AudioComponentDescription defaultOutputDescription;
	defaultOutputDescription.componentType = kAudioUnitType_Output;
	defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
	defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
	defaultOutputDescription.componentFlags = 0;
	defaultOutputDescription.componentFlagsMask = 0;
	
	// Get the default playback output unit
	AudioComponent defaultOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
	NSAssert(defaultOutput, @"Can't find default output");
	
	// Create a new unit based on this that we'll use for output
	OSErr err = AudioComponentInstanceNew(defaultOutput, &toneUnit);
	NSAssert1(toneUnit, @"Error creating unit: %ld", err);
	
	// Set our tone rendering function on the unit
	AURenderCallbackStruct input;
	input.inputProc = RenderTone;
	input.inputProcRefCon = self;
	err = AudioUnitSetProperty(toneUnit, 
		kAudioUnitProperty_SetRenderCallback, 
		kAudioUnitScope_Input,
		0, 
		&input, 
		sizeof(input));
	NSAssert1(err == noErr, @"Error setting callback: %ld", err);
	
	// Set the format to 32 bit, single channel, floating point, linear PCM
	const int four_bytes_per_float = 4;
	const int eight_bits_per_byte = 8;
	AudioStreamBasicDescription streamFormat;
	streamFormat.mSampleRate = sampleRate;
	streamFormat.mFormatID = kAudioFormatLinearPCM;
	streamFormat.mFormatFlags =
		kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
	streamFormat.mBytesPerPacket = four_bytes_per_float;
	streamFormat.mFramesPerPacket = 1;	
	streamFormat.mBytesPerFrame = four_bytes_per_float;		
	streamFormat.mChannelsPerFrame = 1;	
	streamFormat.mBitsPerChannel = four_bytes_per_float * eight_bits_per_byte;
	err = AudioUnitSetProperty (toneUnit,
		kAudioUnitProperty_StreamFormat,
		kAudioUnitScope_Input,
		0,
		&streamFormat,
		sizeof(AudioStreamBasicDescription));
	NSAssert1(err == noErr, @"Error setting stream format: %ld", err);
}

- (IBAction)togglePlay:(UIButton *)selectedButton
{

    NSLog(@"%@",self.toSendData);
	if (toneUnit)
	{
		AudioOutputUnitStop(toneUnit);
		AudioUnitUninitialize(toneUnit);
		AudioComponentInstanceDispose(toneUnit);
		toneUnit = nil;
		
		[selectedButton setTitle:NSLocalizedString(@"Play", nil) forState:0];
	}
	else
	{
        
		[self createToneUnit];
		
		// Stop changing parameters on the unit
		OSErr err = AudioUnitInitialize(toneUnit);
		NSAssert1(err == noErr, @"Error initializing unit: %ld", err);
		
		// Start playback
		err = AudioOutputUnitStart(toneUnit);
		NSAssert1(err == noErr, @"Error starting unit: %ld", err);
		
		[selectedButton setTitle:NSLocalizedString(@"Stop", nil) forState:0];
        
        
	}
    [self.inputTextField resignFirstResponder];
}
- (IBAction)sendData:(id)sender {
    NSTimer * timer = [NSTimer timerWithTimeInterval:0.16 target:self selector:@selector(test) userInfo:nil repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    }
-(void) test{
    if (!isSendingData) {
        
        
        self.toSendData = [@[] mutableCopy];
        self.theta = 0.0;
        NSString * inputChars = [self.inputTextField text];
        for (int charIndex = 0 ; charIndex < [inputChars length]; charIndex++) {
            char innerChar = [inputChars characterAtIndex:charIndex];
            [self.toSendData addObject:@8];
            [self.toSendData addObject:@8];
            [self.toSendData addObject:@8];
            [self.toSendData addObject:@8];
            [self.toSendData addObject:@8];
            [self.toSendData addObject:@8];
            [self.toSendData addObject:@8];
            [self.toSendData addObject:@8];
            [self.toSendData addObject:@2];
            [self.toSendData addObjectsFromArray:[self charToNumberArray:innerChar]];
        }
        isDataReady = YES;
    }

}
- (void)stop
{
	if (toneUnit)
	{
		[self togglePlay:playButton];
	}
}
-(NSMutableArray *) charToNumberArray:(char ) character{
    NSMutableArray * initArray = [@[] mutableCopy];
    for (int index = 0 ; index< 8; index++) {
        if (character & 0x01) {
            [initArray addObject:@1];
        }else{
            [initArray addObject:@0];
        }
        character = character>>1;
    }
    for (int stopIndex=0; stopIndex<8; stopIndex++) {
        [initArray addObject:@8];
    }
    return initArray;
}
- (void)viewDidLoad {
	[super viewDidLoad];
    
    
    isSendingData = NO;
//    self.toSendData = [@[@2,@1,@1,@0,@0,@1,@1,@0,@0,@2,@1,@1,@0,@0,@1,@0,@0,@1] mutableCopy];
    self.toSendData = [@[] mutableCopy];
	[self sliderChanged:frequencySlider];
    
    NSLog(@"%@", [self charToNumberArray:'a']);
	sampleRate = 44100;

	OSStatus result = AudioSessionInitialize(NULL, NULL, ToneInterruptionListener, self);
	if (result == kAudioSessionNoError)
	{
		UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
		AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
	}
	AudioSessionSetActive(true);
}

- (void)viewDidUnload {
    [self setInputTextField:nil];
	self.frequencyLabel = nil;
	self.playButton = nil;
	self.frequencySlider = nil;

	AudioSessionSetActive(false);
}

- (void)dealloc {
    [_inputTextField release];
    [super dealloc];
}
@end
